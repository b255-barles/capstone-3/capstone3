import React from 'react';
import { Row, Col } from 'react-bootstrap';
import fruitsImage from './fruits.jpg';

export default function Highlights() {
  return (
    <div>
      <Row className="mt-3 mb-3">
        <Col xs={12} md={12} style={styles.container}>
          <div style={styles.overlay}></div>
          <img src={fruitsImage} alt="fruits" style={styles.image} />
          <h1 style={styles.text}>FRUIT SHOP</h1>
        </Col>
      </Row>
      <Row>
        <Col style={styles.bottomTextContainer}>
          <p style={styles.bottomText}>
            You don't need to eat less, you just need to eat well
          </p>
        </Col>
      </Row>
    </div>
  );
}

const styles = {
  container: {
    position: 'relative',
    overflow: 'hidden',
  },
  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    zIndex: 1,
  },
  image: {
    width: '100%',
    maxHeight: '400px',
    objectFit: 'cover',
    zIndex: 0,
  },
  text: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    fontSize: '48px',
    fontWeight: 'bold',
    color: 'white',
    textShadow: '2px 2px 4px rgba(0, 0, 0, 0.5)',
    zIndex: 2,
  },
  bottomTextContainer: {
    textAlign: 'center',
    marginTop: '20px',
  },
  bottomText: {
    fontSize: '36px',
    fontWeight: 'bold',
    color: '#edfbd9',
    textShadow: '2px 2px 4px rgba(0, 0, 0, 0.3)',
  },
};
