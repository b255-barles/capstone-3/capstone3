import { Button, Card, Container, Row } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function ProductList({ productProp }) {
  const [stocks, stocksCount] = useState(30);

  const { name, description, price, _id , isActive} = productProp;

  if(!isActive){
    return null;
  }
  return (
    <Container>
      <Row className="justify-content-center">
        <Card className="cardHighlight p-3 mt-5">
          <Card.Body>
            <Card.Title>
              <h2>{name}</h2>
            </Card.Title>

            <Card.Text>Description: {description}</Card.Text>

            <Card.Text style={{ color:'orange'}}>P{price}</Card.Text>

            

            <Link className="btn btn-primary" to={`/productView/${_id}`}>Details</Link>
          </Card.Body>
        </Card>

        {/* Repeat the Card component for the remaining products */}
      </Row>
    </Container>
  );
}
 
ProductList.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  }),
};
