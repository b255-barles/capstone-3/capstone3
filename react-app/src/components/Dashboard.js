import React, { useEffect, useState, useContext } from 'react';
import { Container, Row, Table, Button } from 'react-bootstrap';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Dashboard() {
  const [products, setProducts] = useState([]);
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    // Fetch the products data from the API
    fetch(`${process.env.REACT_APP_API_URL}/products/all`)
      .then((res) => res.json())
      .then((data) => {
        setProducts(data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  const handleToggleActivation = (productId, token) => {
  // Find the product by ID
  const updatedProducts = products.map((product) => {
    if (product._id === productId) {
      // Toggle the isActive property
      const updatedProduct = {
        ...product,
        isActive: !product.isActive,
      };

     const token = localStorage.getItem('token');
      console.log('Token retrieved:', token); // Add this line to check the token value

      fetch(`${process.env.REACT_APP_API_URL}/products/archive/${productId}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}` // Use the retrieved token
        },
        body: JSON.stringify({ isActive: updatedProduct.isActive }),
      })
        .then((res) => res.json())
        .then((data) => {
          console.log('Product updated:', data);
        })
        .catch((error) => {
          console.log(error);
        });

      return updatedProduct;
    }
    return product;
  });

  // Update the products state
  setProducts(updatedProducts);
};
   const handleCreateProduct = () => {
    // Navigate to the createProduct endpoint
    navigate('/createProduct');
  };

  if (!user.isAdmin) {
    return <Navigate to="*" />;
  }

  return (
    <Container className="mt-5">
      <Row className="justify-content-center">
        <Table striped bordered hover style={{ width: '100%',background: 'linear-gradient(135deg, #edfbd9, #d7f9c3, #d7f9c3)' }}>
          <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Price</th>
              <th>Status</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {products.map((product) => (
              <tr key={product._id}>
                <td>{product.name}</td>
                <td>{product.description}</td>
                <td>P{product.price}</td>
                <td>{product.isActive ? 'Active' : 'Inactive'}</td>
                <td>
                  <Link
                    style={{ marginRight: '10px', textDecoration: 'none' }}
                    to={`/updateProduct/${product._id}`}
                  >
                    Edit
                  </Link>
                  <Button
                    variant={product.isActive ? 'danger' : 'success'}
                    onClick={() => handleToggleActivation(product._id)}
                  >
                    {product.isActive ? 'Deactivate' : 'Activate'}
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Row>
      <Row className="justify-content-center mt-2">
        <Button variant="success" onClick={handleCreateProduct}>
          Add Product
        </Button>
      </Row>
    </Container>
  );
}
