// import Button from 'react-bootstrap/Button';
// // Bootstrap grid system components
// import { Row } from 'react-bootstrap';
// import { Col } from 'react-bootstrap';
import {Row, Col} from 'react-bootstrap';


export default function Banner(){
	return (
		<Row>
        	<Col className="p-5">
            
                <h1>Page Not Found</h1>
                <p> Go back to the <a href="/home">  homepage </a> </p>
            </Col>
        </Row>

	)
}