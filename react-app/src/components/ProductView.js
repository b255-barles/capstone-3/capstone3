import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const { productId } = useParams();
  const [product, setProduct] = useState(null);
  const [quantity, setQuantity] = useState(1); // Default quantity is 1

  const order = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/order`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        productId: productId,
        quantity: quantity, // Include the selected quantity in the request
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log('orderData', data);

        if (data === true) {
          Swal.fire({
            title: 'Ordered successfully!',
            icon: 'success',
            text: 'You have successfully ordered',
          });
          navigate('/products');
        } else {
          Swal.fire({
            title: 'Something went wrong',
            icon: 'error',
            text: 'Please try again',
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleQuantityChange = (value) => {
    // Update the quantity state when the value is changed
    setQuantity(value);
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProduct(data);
      });
  }, [productId]);

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          {product && (
            <Card>
              <Card.Body className="text-center">
                <Card.Title>{product.name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{product.description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text style={{ color: 'orange' }}>P{product.price}</Card.Text>
                <Card.Subtitle>Quantity:</Card.Subtitle>
                <div>
                  <Button variant="secondary" onClick={() => handleQuantityChange(quantity - 1)} disabled={quantity <= 1}>
                    -
                  </Button>
                  <span className="mx-2">{quantity}</span>
                  <Button variant="secondary" onClick={() => handleQuantityChange(quantity + 1)}>
                    +
                  </Button>
                </div>
                {user.id !== null ? (
                  <Button variant="primary" block onClick={order}>
                    Order
                  </Button>
                ) : (
                  <Link className="btn btn-danger btn-block" to="/login">
                    Log in to order
                  </Link>
                )}
              </Card.Body>
            </Card>
          )}
        </Col>
      </Row>
    </Container>
  );
}
