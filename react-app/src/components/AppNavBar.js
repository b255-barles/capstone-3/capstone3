import React, { useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext';

export default function AppNavBar() {
  const { user } = useContext(UserContext);

  console.log("admin?", user.isAdmin);
  console.log("name", user)
  return (
    <Navbar className="navbar-bg" expand="lg">
      <Container fluid>
        <Navbar.Brand as={Link} to="/home" exact="true">Fruit Shop</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={NavLink} to="/home" exact="true" activeClassName="active">
              Home
            </Nav.Link>
            <Nav.Link as={NavLink} to="/products" exact="true" activeClassName="active">
              Products
            </Nav.Link>
            {user.isAdmin && (
              <Nav.Link as={NavLink} to="/dashboard" exact="true" activeClassName="active">
                Dashboard
              </Nav.Link>
            )}
          </Nav>
          <Nav className="ms-auto me-0">
            {user.id !== null ? (
              <>
                <Nav.Link >
                  Welcome, {user.firstName} {user.lastName}
                </Nav.Link>
                <Nav.Link as={NavLink} to="/logout" exact="true" activeClassName="active">
                  Logout
                </Nav.Link>
                
              </>
            ) : (
              <>
                <Nav.Link as={NavLink} to="/login" exact="true" activeClassName="active">
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register" exact="true" activeClassName="active">
                  Register
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>

      <style>
        {`
          .navbar-bg {
            background: linear-gradient(135deg, #edfbd9, #d7f9c3, #d7f9c3);
            box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.1);
          }
          .navbar-bg::before,
          .navbar-bg::after {
            content: '';
            position: absolute;
            top: 0;
            height: 100%;
            width: 20px;
            background-color: rgba(255, 255, 255, 0.05);
            z-index: -1;
          }
          .navbar-bg::before {
            left: 0;
          }
          .navbar-bg::after {
            right: 0;
          }
          .navbar-brand {
            color: black;
            font-size: 1.5rem;
            font-weight: bold;
          }
          .navbar-nav .nav-link:hover,
          .navbar-nav .nav-link.active {
            color: black;
            background-color: rgba(255, 255, 255, 0.1);
            border-radius: 5px;
          }
          .navbar-nav .nav-link:hover,
          .navbar-nav .nav-link:focus {
            background-color: #adf97e;
            border-radius: 5px;
          }
          .active {
            font-weight: bold;
          }
        `}
      </style>
    </Navbar>
  );
}