import { Fragment, useEffect, useState } from 'react'
import ProductList from '../components/ProductList'

export default function Products(){
	
	
	const [products, setProduct] = useState([])

	useEffect(() => {

						fetch(`${process.env.REACT_APP_API_URL}/products/all`)
						.then(res => res.json())
						.then(data => {
						    
						    console.log(data);

						    // Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" components
						    setProduct(data.map(product => {
						        return (
						            <ProductList key={product._id} productProp={product}/>
						        );
						    }));

						});

			    }, []);

	

	return(
		<Fragment>
			{products}
		</Fragment>
	)
	
}