import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import backgroundImage from '../components/fruits.jpg'; 

export default function Login(props) {
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState('');

  function loginUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
      .then(res => res.json())
      .then(data => {
        console.log('L',data)
        if (typeof data.access !== 'undefined') {
          localStorage.setItem('token', data.access);
          Swal.fire({
            icon: 'success',
            title: 'Success!',
            text: 'You have been logged in.',
          });
          return retrieveUserDetails(data.access);
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Error!',
            text: 'Invalid email or password.',
          });
        }
      })
      .catch(error => {
        // Error handling...
      });

    setEmail('');
    setPassword('');
  }

  const retrieveUserDetails = (token) => {
    return fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setUser({
          firstName: data.firstName,
          lastName: data.lastName,
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
        backgroundImage: `url(${backgroundImage})`, // Use the imported image here
        backgroundSize: 'cover',
      }}
    >
      {user.id !== null ? (
        <Navigate to="/" />
      ) : (
        <Form style={{ background: '#edfbd9' }} className="mt-3 p-5" onSubmit={e => loginUser(e)}>
          <Form.Group controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={e => setEmail(e.target.value)}
              required
              style={{ width: '300px' }}
            />
          </Form.Group>

          <Form.Group controlId="userPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              onChange={e => setPassword(e.target.value)}
              required
              style={{ width: '300px' }}
            />
          </Form.Group>


          <br />
          <div style={{textAlign:'center'}}>
          {isActive ? (
            <Button  variant="primary" type="submit" id="submitBtn">
              Submit
            </Button>
          ) : (
            <Button variant="primary" type="submit" id="submitBtn" disabled>
              Submit
            </Button>
          )}
          </div>
        </Form>
      )}
    </div>
  );
}
