import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function Register(){

    	// State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    
    const [mobileNo, setMobileNo] = useState('')

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether submit vuttion is enabled or not
	const [isActive, setIsActive] = useState(false);
  
    const navigate = useNavigate();

    const { user } = useContext(UserContext);
	
    
	// Function to simulate user registration
	function registerUser(e){
		//  Prevents page redirection via form submission
		e.preventDefault();

               
            fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    // Authorization: `Bearer ${ localStorage.getItem('token') }`
                },
                body: JSON.stringify({
                    email: email,
                }),
            })
            .then((res) => res.json())
            .then((data) => {
                
                console.log(data)
                if(data === true){
                    Swal.fire({
                        title: "Email Exist",
                        icon: 'error',
                        text: 'Email Already used by another user, try using another email'
                    });
                    
                } else {
                    // if email does not exist

                    console.log("name", email)
                    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                           
                        },
                        body: JSON.stringify({
                            firstName: firstName,
                            lastName: lastName,
                            email: email,
                            password: password2,
                            mobileNo: mobileNo
                            
                        }),

                    })
                    .then((res) => res.json())

                    .then((data) => {
                        
                        console.log("reg", data)
                        if(data === true){
                            Swal.fire({
                                title: "Registered Successfully!",
                                icon: 'success',
                                text: `Thank you for Registering ${firstName}!`
                            });
                            navigate("/login");
                        } else {
                            Swal.fire({
                                title: "Registration Failed",
                                icon: 'error',
                                text: ' Sorry for the Inconvenience, please try again'
                        });
                    
                    

                }
            });
        }
    });


		// clear input fields
        setFirstName('');
        setLastName('');
		setEmail('');
		setPassword1('');
		setPassword2('');
        setMobileNo('');

		

     
    }

	useEffect(() => {
        // Validation to enable submit button when all fields are populated and both passwords match
        if((email !== '' && password1 !== '' && password2 !== '') && ( password1 === password2)){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password1, password2])

   

	return (

        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
                <div style={{ textAlign: 'center' }}>
    
                <h2 style={{fontWeight: 'bold'}}><u> REGISTER HERE</u></h2>



       {/*(user.id !== null) ?
            <Navigate to = "/courses" />
        :*/}
		<Form style={{background:'#edfbd9', textAlign: 'left'}}  className = "p-5" onSubmit={(e) => registerUser(e)} >

            <Form.Group controlId="firstName">
                <Form.Label>First Name: </Form.Label>
                <Form.Control 
                    type="first name" 
                    placeholder="Enter First Name" 
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                    required
                    style={{ width: '300px' }}
                />
               
            </Form.Group>

            <Form.Group controlId="lastName">
                <Form.Label>Last Name: </Form.Label>
                <Form.Control 
                    type="last name" 
                    placeholder="Enter Last Name" 
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                    required
                    style={{ width: '300px' }}
                />
               
            </Form.Group>



            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                    style={{ width: '300px' }}
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="mobileNo">
                <Form.Label>Mobile Number: </Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter Mobile Number" 
                    value={mobileNo}
                    onChange={e => setMobileNo(e.target.value)}
                    required
                    minLength={11}
                    style={{ width: '300px' }}
                />
               
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                    required
                    style={{ width: '300px' }}
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                    style={{ width: '300px' }}
                />
            </Form.Group>

            

           

            <br />
            <div style={{textAlign:'center'}}>
            {isActive ? 
            
            <Button variant="primary" type="submit" id="submitBtn">
                Submit
            </Button>
            :
            <Button variant="primary" type="submit" id="submitBtn" disabled>
                Submit
            </Button>
        	}
            </div>

        </Form>
        </div>
        </div>


	)
};


