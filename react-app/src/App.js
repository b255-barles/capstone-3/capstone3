import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';
import React from 'react';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Products from './pages/Products';
import ProductView from './components/ProductView';
import CreateProduct from './components/CreateProduct';
import UpdateProduct from './components/UpdateProduct';
// import Destinations from './pages/Destinations';
import Dashboard from './components/Dashboard';
import Error from './components/Error';
import { useState, useEffect } from 'react';
import './App.css';
import { UserProvider } from './UserContext';

function App() {
  const [user, setUser] = useState({
    firstName: null,
    lastName: null,
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.clear();
  };

 useEffect(() => {
     console.log("user", user);
     console.log("local", localStorage);
   }, [user]);



  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <div className="app-container mr-auto">
          <div className="gradient-background"></div>
          <Container fluid>
            <AppNavBar className= "sticky-top" />
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/home" element={<Home />} />

              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/products" element={<Products />} />
              <Route path="/productView/:productId" element={<ProductView />} />
              <Route path="/dashboard" element={<Dashboard/>} />
              <Route path="/updateProduct/:productId" element={<UpdateProduct/>} />
              <Route path="/createProduct" element={<CreateProduct/>} />
              <Route path="*" element={<Error/>} />

            </Routes>
          </Container>
        </div>
      </Router>
      <style>
        {`
          .app-container {
            position: relative;
            z-index: 1;
          }

          .gradient-background {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: linear-gradient(45deg, #00b09b, #96c93d);
            z-index: -1;
          }
        `}
      </style>
    </UserProvider>
  );
}

export default App;
