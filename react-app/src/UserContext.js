import React from 'react';

// Creates a context object
// A context object as the namestates is a  data type of an object that can be used to store info that can be shared to other components within this app
// The context object is a different approach to passing information between components and allows easier access by avoiding the use of prop-drilling
const userContext = React.createContext();

// The "provider" component allows othe component to consume/use the context object and supply the necessary information needed to the context object

export const UserProvider = userContext.Provider;

export default userContext;